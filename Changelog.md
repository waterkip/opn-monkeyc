Changelog for OPN-MonkeyC
===

## v0.0.6 Currently in development

  * Add OPN.X11.View.ViewX  
    This is a basic view that deals with the most common pitfalls in dealing
    with views: When to call `View.onUpdate(dc)`. And this class deals with it
    simply by only calling it when has a layout.  It also supports refreshing
    of the screen and exposes the `dcWidth` and `dcHeight` after asking it only
    once. See the documenation on how to use it.

  * Add `seconds2HMS()` for seconds to hh:mm:ss calculations.  
    This is needed when you do math with time as the diff between two durations
    in done in seconds.

  * Add non-animated banners to OPN.X11.Layer as OPN.X11.Layer.Banner


## v0.0.5 2022-07-26T16:57:41-04:00

  This release is dedicated to math

  * Get x/y coordinates for a square on your watch in any size

  * Introduce `abs()` and `max()` functions in OPN.Math.Util

  * Move `seed_rng()` and `randomChar()` to OPN.Math.Util  
    This moves make the `:uuid` annotation depend on the `:math` annotation
    It also deprecates the use of seed_rng and randomChar() in the UUID module.
    Please use the Math versions instead.

  * Introduce OPN.X11.Layer.AnimatedBanner

  * Create documentation with naturaldocs (v1.52)

## v0.0.4 2022-07-22T15:02:54-04:00

  BREAKING CHANGES
  * Rename OPN.Conversion.Garmin to OPN.Math.Coordinates


## v0.0.3 2022-07-21T14:27:58-04:00

  This release now includes the following new shiney gadgets:
  * UUIDv4 creation
  * Menu2 classes
  * Compare arrays with `is()`
  * And get x/y coordinates on a circle in any size on your watch

## v0.0.2 2022-07-07T19:11:41-04:00

  Improve OPN.Test.More
  * Implement `threw()` test methods to test exceptions
  * Add `diag()` method to OPN.Test.More
  * Implement the ability to test nulls with `is()`

  Developer only
  * Make barrel typecheck 1 compatible

## v0.0.1 2022-07-03T09:54:15-04:00

  Implement OPN.Test.More

  * Adds test functions is/isnt/ok/pass/fail/instanceOf and isa

  Implement OPN.Conversion modules.

  * Convert time, distance, temperature and weight from ISO to imperial units

