//
//SPDX-FileCopyrightText: 2021-2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//
using Toybox.Test as t;
import Toybox.Lang;
using Toybox.System as Sys;

/*

 Module: OPN.Test.More

 Description:
 This module is an superset of Toybox.Test. It does not extend it, nor can you
 use it as a drop-in replacement. But you can use it instead of. General usage
 would be:

 > using OPN.Test.More as t;
 > module MyTest {
 >   (:test)
 >   function mytest(logger) {
 >     // Test code here
 >     t.is(1, 1)
 >     return true;
 >   }
 > }

 Annotation: test

*/

module OPN {

  (:test)
  module Test {


    module More {

      // Function: diag(msg)
      // Outputs the message on the console
      function diag(msg) {
        Sys.println("# " + msg);
      }

      // Function: ok(needle, msg)
      // Wrapper around Toybox.Test.assertmessage(needle, msg)
      function ok(needle, msg) {
        t.assertMessage(needle, msg);
      }

      // Function: pass(msg)
      // Your test always passes
      function pass(msg) {
        ok(true, msg);
      }

      // Function: fail(msg)
      // Your test always fails
      function fail(msg) {
        ok(false, msg);
      }

      // Function: is(needle, haystack, msg)
      // Compares the needle and the haystack and fails if they aren't equal.
      //
      // Please note:
      // * Doubles are tested up to their 10th precision
      // * Arrays are tested by comparing their toString() output
      function is(needle, haystack, msg) {
        if (haystack == null && needle == null) {
          pass(msg);
          return;
        }
        else if (haystack == null || needle == null) {
          fail(msg);
          return;
        }
        if (needle instanceof Lang.Double) {
          needle   = needle.format("%.10g");
          haystack = haystack.toDouble().format("%.10g");
        }

        if (haystack instanceof Lang.Array) {
          // !foo instanceof Something is a type error
          if (needle instanceof Lang.Array == false) {
            Sys.println("Needle isn't an Toybox.Lang.Array");
            fail(msg);
            return;
          }

          if (needle.size() != haystack.size()) {
            Sys.println(Lang.format("Got $1$ items, expected $2$ items",
              [ needle.size(), haystack.size()]
            ));
            fail(msg);
            return;

          }
          needle   = needle.toString();
          haystack = haystack.toString();
        }

        try {
          t.assertEqualMessage(needle, haystack, msg);
        }
        catch (e) {
          Sys.println(Lang.format("Got: $1$\nExpect: $2$\n", [ needle, haystack ]));
          throw e;
        }
      }

      function _is(needle, haystack) {
        if (haystack == null && needle == null) {
          return;
        }
        else if (haystack == null || needle == null) {
          t.assert(false);
        }

        if (needle instanceof Lang.Double) {
          needle = needle.format("%.10g");
          haystack = haystack.toDouble().format("%.10g");
        }
        try {
          t.assertEqual(needle, haystack);
        }
        catch (e) {
          Sys.println(Lang.format("Got: $1$\nExpect: $2$\n", [ needle, haystack ]));
          throw e;
        }
      }

      // Function: isnt(needle, haystack, msg)
      // Compares the needle and the haystack and fails if they are equal.
      //
      // Please note:
      // * Doubles are tested up to their 10th precision
      // * Arrays are not supported
      function isnt(needle, haystack, msg) {
        if (haystack == null && needle == null) {
          fail(msg);
          return;
        }
        else if (haystack == null || needle == null) {
          pass(msg);
          return;
        }
        if (needle instanceof Lang.Double) {
          needle = needle.format("%.10g");
          haystack = haystack.toDouble().format("%.10g");
        }
        try {
          t.assertNotEqualMessage(needle, haystack, msg);
        }
        catch (e) {
          Sys.println(Lang.format("Got: $1$\nShould not be: $2$\n", [ needle, haystack ]));
          throw e;
        }
      }

      // Function: isa(needle, instance, msg)
      // Tests if your needle is-a instance
      function isa(needle, instance, msg) {
        instanceOf(needle, instance, msg);
      }

      // Function: instanceOf(needle, instance, msg)
      // Alias for isa()
      function instanceOf(needle, instance, msg) {
        ok(needle instanceof instance, msg);
      }

      // Function: can(needle, symbol, msg)
      // Deprecated: This function will be removed in an upcoming release, use
      // can_ok instread
      // Tests if your needle is an instance of instance
      function can(needle, haystack as Lang.Symbol, msg) {
        ok(needle has haystack, msg);
      }
      // Function: can_ok(needle, symbol, msg)
      // Tests if your needle is an instance of instance
      function can_ok(needle, haystack as Lang.Symbol, msg) {
        ok(needle has haystack, msg);
      }


      /*
        I would love to have throws_ok(cb, type, error, msg);
        But the problem is that you need to do something like this:

        ```
        var x = new Foo();
        var cb = x.method(:methodName);

        t.throwsOk(cb.invoke(args), ....);
        t.throwsOk(cb, invoke(args), ....);
        ```

        But we can't because we don't have variable params in monkeyC
        And there aren't any lambda's or anonymouse functions in monkeyC as
        well afaik.

      */

      // Function: threw(exception, type, errormsg, msg)
      // Deprecated: This function will be removed in an upcoming release, use
      // threw_ok instread
      //
      // Tests if an exception is of the given type and if the error message it
      // correct.
      function threw(e, type, error, msg) {
        try {
          t.assert(e instanceof type);
          _is(e.getErrorMessage(), error);
        }
        catch (er) {
          fail(msg);
        }
      }

      // Function: threw_ok(exception, type, errormsg, msg)
      // Tests if an exception is of the given type and if the error message it
      // correct.
      function threw_ok(e, type, error, msg) {
        try {
          t.assert(e instanceof type);
          _is(e.getErrorMessage(), error);
        }
        catch (er) {
          fail(msg);
        }
      }
    }
  }
}
