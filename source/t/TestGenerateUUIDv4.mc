//
//SPDX-FileCopyrightText: 2021-2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//
using OPN.Test.More as t;
using OPN.Generate.UUID as UUID;
using Toybox.Math;
using Toybox.System as Sys;
import Toybox.Lang;

module OPN {

  (:test)
  function testUUID(logger) {

    var uuids = [
      "fa642367-9fd8-495c-b618-5fa487ce4baf", // our barrel ID
      "ff55ff21-691d-4498-9c96-817776dbbe5a",
      "1290be25-e80a-4e72-8f9c-7ef2fe8feda7",
      "6191eee1-6725-4f56-ba77-ce86ba387006",
      "cc1d0711-ed6e-4c63-ab8a-bd44c395896d"
    ] as Array<Lang.String>;

    for (var i = 0; i< uuids.size(); i++) {
      var uuid = uuids[i];
      t.ok(UUID.validate_uuid_v4(uuid), uuid + " is validated");
    }

    var invalids = [
      "5aa22016-10da-11ea-9a9f-362b9e155667", // v1
      "cc1d0711-ed6e-4c63-ed8a-bd44c395896d", // incorrect variant
    ] as Array<Lang.String>;

    for (var i = 0; i< invalids.size(); i++) {
      var uuid = invalids[i];
      t.ok(!UUID.validate_uuid_v4(uuid), uuid + " is not validated");
    }

    UUID.seed_rng();

    t.ok(UUID.validate_uuid_v4(UUID.generate_v4()), "The UUID is correct!");

    t.diag(UUID.uuid_v4_hex());
    t.diag(UUID.uuid_v4_string());
    t.diag(UUID.uuid_v4_slug());

    return true;
  }
}
