//
//SPDX-FileCopyrightText: 2021-2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//
using OPN.Test.More as t;
using OPN.Conversion.Time as tt;

module OPN {

  (:test)
  class MockClock {

    public var hour = 12;
    public var min  = 12;
    public var sec  = 12;

    function initialize(hour, min, sec) {
      self.hour = hour;
      self.min  = min;
      self.sec  = sec;
    }
  }

  (:test)
  function testTime(logger) {
    var now = new MockClock(12, 12, 12);
    t.is(tt.iso8601(now), "12:12:12", "It is the correct time");
    t.is(tt.toAmPm(now), "12:12:12 pm", ".. also in am/pm");

    now = new MockClock(0, 12, 13);
    t.is(tt.iso8601(now), "00:12:13", "It is the correct time at mid night");
    t.is(tt.toAmPm(now), "12:12:13 am", ".. also in am/pm");

    now = new MockClock(1, 12, 13);
    t.is(tt.iso8601(now), "01:12:13", "One in the morning");
    t.is(tt.toAmPm(now), "1:12:13 am", ".. also in am/pm");

    now = new MockClock(13, 12, 13);
    t.is(tt.iso8601(now), "13:12:13", "One in the afternoon");
    t.is(tt.toAmPm(now), "1:12:13 pm", ".. also in am/pm");

    return true;
  }

  (:test)
  function testDuration(logger) {
    t.is(tt.seconds2HMS(5), "00:00:05", "5 seconds");
    t.is(tt.seconds2HMS(65), "00:01:05", "1 min, 5 seconds");
    t.is(tt.seconds2HMS(901), "00:15:01", "901 seconds");
    t.is(tt.seconds2HMS(901 * 4), "01:00:04", "501 * 4 seconds");
    t.is(tt.seconds2HMS(901 * 5), "01:15:05", "501 * 5 seconds");
    t.is(tt.seconds2HMS(3601 * 24), "24:00:24", "501 * 4 seconds");
    return true;

  }
}
