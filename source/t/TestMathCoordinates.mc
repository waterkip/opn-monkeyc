//SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opperschaap.net>
//SPDX-FileCopyrightText: 2022 Nichole Danser <nichole@scubblebubbles.org>
//
//SPDX-License-Identifier: BSD-3-Clause

using OPN.Test.More as t;
using OPN.Math.Coordinates as g;
import Toybox.Lang;

module OPN {

  (:test)
  function testGetCoordinates(logger) {
    /*
      x=140, y=0   is the 12 o clock
      x=280, y=140 is the 3 o clock
      x=140, y=280 is the 6 o clock
      x=0,   y=140 is the 9 o clock
    */

    var screen = 280;
    var begin  = 0.toDouble();
    var end    = screen.toDouble();
    var middle = 140.toDouble();

    t.is(g.getCircleCoords({:size => 280, :degrees =>  0}),
      [ middle, begin ], "Got the correct coordinates for 0 degrees");

    t.is(g.getCircleCoords({:size => screen, :degrees => -360}),
      [ middle, begin ], "Got the correct coordinates for -360 == 0 degrees");

    t.is(g.getCircleCoords({:size => screen, :degrees => 360}),
      [ middle, begin ], "Got the correct coordinates for 360 == 0 degrees");

    var x = g.getCircleCoords({:size => screen, :degrees => 60});
    t.is(x[1], 70d, "y at 60 == 70");

    t.is(g.getCircleCoords({:size => screen, :degrees => 90}),
      [ end, middle ], "Got the correct coordinates for 90 degrees");

    t.is(g.getCircleCoords({:size => screen, :degrees => 180}),
      [ middle, end ], "Got the correct coordinates for 180 degrees");

    t.is(g.getCircleCoords({:size => screen, :degrees => 270}),
      [ begin, middle ], "Got the correct coordinates for 270 degrees");

    t.is(g.getCircleCoords({:size => screen, :degrees => 90 + 360}),
      [ end, middle ], "Got the correct coordinates for 90 + 360 degrees");

    t.is(g.getCircleCoords({:size => screen, :degrees => 90 - 360}),
      [ end, middle ], "Got the correct coordinates for 90 - 360 degrees");

    return true;
  }

  (:test)
  function testGetCoordinatesOfBox(logger) {
    /*
      x=140, y=0   is the 12 o clock
      x=280, y=140 is the 3 o clock
      x=140, y=280 is the 6 o clock
      x=0,   y=140 is the 9 o clock
    */

    var screen = 280;
    var begin  = 0.toDouble();
    var end    = screen.toDouble();
    var center = (screen / 2).toDouble();

    var tests = [
      { 0  =>  [ center, begin ] },
      { 10 =>  [ 164.685777d, begin ] },
      { 45 =>  [ end, begin ] },
      { 60 =>  [ end, 59.170962d ] },
      { 90 =>  [ end, center ] },
      { 95 =>  [ end, 152.248413d ] },
      { 175 => [ 152.248413d, end ] },
      { 180 => [ center, end ] },
      { 190 => [ 115.314223d, end ] },
      { 250 => [ begin, 190.955833d ] },
      { 270 => [ begin, center ] },
      { 280 => [ begin, 115.314223d ] },
      { 315 => [ begin, begin ] },
      { 322 => [ 30.620012d, begin ] },
    ];

    var xy;
    var key;

    for (var i = 0; i < tests.size(); i++) {
      key = (tests[i] as Dictionary).keys()[0];
      xy  = g.getBoxCoords({:size => screen, :degrees =>  key});
      t.is(xy, tests[i].get(key), key + " degrees has correct coords");
    }

    return true;
  }
}
