//
//SPDX-FileCopyrightText: 2021-2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//
using OPN.Test.More as t;
using OPN.Conversion.Distance as dist;

module OPN {

  (:test)
  function testDistance(logger) {

    t.is(dist.toMiles(1), 0.6213711922d, "1km is 0.62137 miles");
    t.is(dist.toMiles(21.0975), 13.1093786326d, "Half marathon distance");

    return true;
  }
}
