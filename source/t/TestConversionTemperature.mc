//
//SPDX-FileCopyrightText: 2021-2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//
using OPN.Test.More as t;
using OPN.Conversion.Temperature as temp;

module OPN {

  (:test)
  function testTemp(logger) {
    t.is(temp.toF(23), 73.4d, "Centrigrade to celcius is correct");
    t.is(temp.toF(31), 87.8d, ".. also for Aruba temp");
    t.is(temp.toF(0), 32d, ".. also for freezing");
    t.is(temp.toF(-10), 14d, ".. also for really freezing");
    return true;
  }
}
