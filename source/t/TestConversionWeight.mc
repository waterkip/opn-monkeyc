//
//SPDX-FileCopyrightText: 2021-2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//
using OPN.Test.More as t;
using OPN.Conversion.Weight as mass;

module OPN {

  (:test)
  function testWeight(logger) {
    t.is(mass.toLbs(1), 2.2046226218d, "1kg is 2.20462 lbs");
    t.is(mass.toLbs(86.2), 190.0384632754d, "Karting weight");
    t.is(mass.toLbs(20), 44.0924524370d, "Barbell weight");
    return true;
  }
}
