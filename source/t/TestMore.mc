//
//SPDX-FileCopyrightText: 2021-2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//
using OPN.Test.More as t;
import Toybox.Lang;
import Toybox.Test;

module OPN {

  (:test)
  function testTestMore(logger) {
    t.is(null, null, "We are null");

    try {
      t.is(null, "foo", "One of us is null");
    }
    catch (e) {
      t.threw(e,
        Toybox.Test.AssertException, "ASSERTION FAILED: One of us is null",
        "Correct failure for null != null");
    }

    t.isnt(null, "foo", "One of us is null");
    try {
      t.isnt(null, null, "We aren't null");
    }
    catch (e) {
      t.threw(e,
        Toybox.Test.AssertException, "ASSERTION FAILED: We aren't null",
        "Correct failure for null != null");
    }

    t.is([0, 140], [0, 140], "Arrays equal too");
    return true;
  }
}
