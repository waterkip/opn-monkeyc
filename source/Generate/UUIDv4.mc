// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause
using Toybox.System as Sys;
import Toybox.Lang;
import Toybox.StringUtil;
using Toybox.Math;
using OPN.Math.Util;

// Module: OPN.Generate.UUID

// Annotation: uuid

module OPN {

  (:uuid)
    module Generate {

      module UUID {

        function seed_rng() {
          return OPN.Math.Util.seedRNG();
        }

        function randomChar() {
          return OPN.Math.Util.randomChar();
        }

        function _uuid_v4_bytes() as Lang.ByteArray {
        /* https://www.cryptosys.net/pki/uuid-rfc4122.html
           The procedure to generate a version 4 UUID is as follows:

           * Generate 16 random bytes (=128 bits)
           * Adjust certain bits according to RFC 4122 section 4.4 as follows:
             * set the four most significant bits of the 7th byte to 0100'B, so the high nibble is "4"
             * set the two most significant bits of the 9th byte to 10'B, so the high nibble will be one of "8", "9", "A", or "B" (see Note 1).
           * Encode the adjusted bytes as 32 hexadecimal digits
           * Add four hyphen "-" characters to obtain blocks of 8, 4, 4, 4 and 12 hex digits
           * Output the resulting 36-character string "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"

         */

          var r = randomChar().toString();
          while (r.length() < 16) {
            r += randomChar().toString();
          }

          var bytes = StringUtil.convertEncodedString(
            r,
            {
              :fromRepresentation => StringUtil.REPRESENTATION_STRING_PLAIN_TEXT,
              :toRepresentation => StringUtil.REPRESENTATION_BYTE_ARRAY,
            }
          ) as Lang.ByteArray;

          bytes[6] = (bytes[6] | 0x40) & 0x4F;
          bytes[8] = (bytes[8] | 0x80) & 0xBF;
          return bytes;

        }

        // Function: uuid_v4_slug()
        // Returns the slug of a UUID v4. This is not something that can be
        // read by humans tho. But merely all the characters are stringified.
        function uuid_v4_slug() as Lang.String {
          var bytes = _uuid_v4_bytes();
          var str = "";
          for (var i=0;i<bytes.size();i++) {
            str += bytes[i].toChar().toString();
          }
          return str;
        }

        // Function: uuid_v4_hex()
        // Returns the hex value of a UUID v4.
        function uuid_v4_hex() as Lang.String {
          var bytes = _uuid_v4_bytes();
          return StringUtil.convertEncodedString(
            bytes,
            {
              :fromRepresentation => StringUtil.REPRESENTATION_BYTE_ARRAY,
              :toRepresentation => StringUtil.REPRESENTATION_STRING_HEX,
            }
          );
        }

        // Function: uuid_v4_hex2string(hex)
        // Returns the UUID v4 as most people know it
        function uuid_v4_hex2string(strHex) {
          return Lang.format("$1$-$2$-$3$-$4$-$5$", [
            strHex.substring(0,8), strHex.substring(8,12),
            strHex.substring(12,16), strHex.substring(16,20),
            strHex.substring(20,32)
          ]);
        }

        // Function: uuid_v4_string()
        // Get a UUID v4 as a string
        function uuid_v4_string() as Lang.String {
          return uuid_v4_hex2string(uuid_v4_hex());
        }

        // Function: generate_v4()
        // The same as uuid_v4_string()
        function generate_v4() {
          return uuid_v4_string();
        }

        // Function: validate_uuid_v4()
        // Validates a UUID v4 string
        function validate_uuid_v4(uuid as Lang.String) as Boolean {

          var v4 = uuid.substring(14,15);
          if (!v4.equals("4")) {
            return false;
          }

          var maybe = uuid.substring(19,20);
          var variants = ["a", "b", "8", "9"] as Array<Lang.String>;
          var ok = false;
          for (var i = 0; i<variants.size(); i++) {
            if (maybe.equals(variants[i])) {
              ok = true;
              break;
            }
          }
          if (ok == false) {
            return false;
          }

          var strHex = Lang.format("$1$$2$$3$$4$$5$", [
              uuid.substring(0,8), uuid.substring(9,13),
              uuid.substring(14,18), uuid.substring(19,23),
              uuid.substring(24,36)
          ]);

          var bytes = StringUtil.convertEncodedString(
            strHex,
            {
              :toRepresentation => StringUtil.REPRESENTATION_BYTE_ARRAY,
              :fromRepresentation => StringUtil.REPRESENTATION_STRING_HEX,
            }
          ) as Lang.ByteArray;

          for (var i = 0; i< bytes.size(); i++) {
            var c = bytes[i] as Lang.Char;
            var n = c.toNumber();
            if (n < 0 || n > 255) {
              return false;
            }
          }

          // Check if version is 4 (bits 12-15 set to 0100)
          if ((bytes[6] & 0xF0) != 0x40) {
              return false;
          }

          // Check if variant is valid (bits 6-7 set to 10)
          if ((bytes[8] & 0xC0) != 0x80) {
              return false;
          }

          return true;
        }
      }
    }
}
