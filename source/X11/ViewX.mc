// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi;
import Toybox.Lang;
import Toybox.Timer;
import Toybox.Graphics;

module OPN {

  (:gfx)
  module X11 {

    module View {
      //
      // Class: ViewX
      // A very basic view
      //
      // Parameters:
      // - args (Lang.Dictionary) or null
      //
      // Symbols for args:
      // - :refresh,  the interval for each refresh
      // - :layout,   The symbol to the layout
      // - :background, Background color, defaults to black
      // - :foreground, Foreground color, defaults to white
      //
      // Description:
      //
      // A basic view, which takes care of the specific logic for you so you
      // don't need to remember if you have a layout or not. Classes do not
      // need to extend/override the onUpdate(dc) call, this class takes care
      // of that. In order to do that its children must implement all or none
      // of the following methods:
      //
      //   * When using a layout onUpdateLayout()
      //   * When using the Dc onUpdateCanvas(dc)
      //
      // Example:
      //
      // > using OPN.X11.View as V;
      // > class MyView extends V.ViewX {
      // >    function initialize() {
      // >      // Refresh the view every second
      // >      V.ViewX.initialize({
      // >        :refresh => 1000,
      // >        :layout  => :Layout, // This resolves to Rez.Layouts.Layout
      // >        :background => Graphics.COLOR_BLUE,
      // >        :foreground => Graphics.COLOR_YELLOW,
      // >      });
      // >    }
      // >
      // >    protected function onUpdateLayout() {
      // >      // Update your layout here
      // >    }
      // >    protected function onUpdateCanvas() {
      // >      // Update your canvas here
      // >    }
      // > }
      //
      class ViewX extends Toybox.WatchUi.View {


        // Attributes:
        // * dcWidth
        // * dcHeight
        // * dcColorFg
        // * dcColorBg
        protected var dcWidth;
        protected var dcHeight;
        protected var dcColorFg;
        protected var dcColorBg;

        // I don't see a reason for these to be exposed
        // Refresh might be usefull to get quicker refreshes on a particular
        // view but might want to do that with getter/setters to get better
        // defaults
        private var timer;

        // Layout/refresh are special because the typechecker complains about:
        // WARNING: fenix6xpro: ViewX.mc Unable to detect scope for the symbol
        // reference 'layout'.
        // So we privatize them, for profit ofc
        private var _refresh;
        private var _layout;

        function initialize(args as Lang.Dictionary?) {
          WatchUi.View.initialize();
          self.dcWidth = null;
          self.dcHeight = null;

          if (args == null) {
            args = {};
          }

          if (args.hasKey(:refresh)){
            self.timer = new Timer.Timer();
            self._refresh = args.get(:refresh);
            if (self._refresh == null || self._refresh < 50) {
              self._refresh = 1000;
            }
          }
          else {
            self.timer   = null;
            self._refresh = null;
          }

          self._layout = args.hasKey(:layout) ? args.get(:layout) : null;

          self.dcColorBg = args.hasKey(:background) ?
            args.get(:background) : Graphics.COLOR_BLACK;
          self.dcColorFg = args.hasKey(:foreground) ?
            args.get(:foreground) : Graphics.COLOR_WHITE;

        }

        function onLayout(dc) {
          self.dcWidth  = dc.getWidth();
          self.dcHeight = dc.getHeight();
          if (self._layout != null) {
            var m = new Lang.Method(Rez.Layouts, self._layout);
            setLayout(m.invoke(dc));
          }
        }

        // Function: onUpdate(dc)
        // Takes care of the onUpdate(dc) calls for you
        // There is no need to override this method as it deals with most of
        // the logic for you.
        function onUpdate(dc) {
          if (self._layout != null) {
            onUpdateLayout();
            WatchUi.View.onUpdate(dc);
          }
          else {
            dc.setColor(self.dcColorFg, self.dcColorBg);
            dc.clear();
          }
          onUpdateCanvas(dc);

          if (self.timer != null) {
            self.timer.start(new Lang.Method(Toybox.WatchUi, :requestUpdate), self._refresh, false);
          }
        }

        // Function: onUpdateLayout()
        // This function is responsible to update the layout if you have one.
        // By default it does nothing. So your XML stays unaltered. If you need
        // to change your layout, this is the place to do it.
        protected function onUpdateLayout() {
          return;
        }

        // Function: onUpdateCanvas(dc)
        // This function is responsible to update the canvas if you have one.
        // By default it does nothing. Make sure the clear the dc first
        protected function onUpdateCanvas(dc) {
          return;
        }
      }
    }
  }
}
