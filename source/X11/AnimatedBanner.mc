// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi as Ui;
import Toybox.Lang;
using Toybox.Timer;

// Module: OPN.X11.Layer

// Annotation: gfx

module OPN {

  (:gfx)
  module X11 {

    module Layer {


      // Class: AnimatedBanner
      //
      // Parameters:
      //  - :visibility (default false)
      //  - :slide      (default WatchUi.SLIDE_DOWN)
      //  - :speed      (default to 50)
      //  - :dc         (required)
      //  - :steps      (default to 7)
      class AnimatedBanner extends AnimatedBaseBanner {

        function initialize(args) {

          if (!args.hasKey(:slide)) {
            args.put(:slide, Ui.SLIDE_DOWN);
          }
          AnimatedBaseBanner.initialize(args);

          if (self.slide == Ui.SLIDE_UP || self.slide == Ui.SLIDE_LEFT) {
            if (!args.hasKey(:dc)) {
              throw new Lang.InvalidOptionsException(
                "We require a dc object on anything except WatchUi.SLIDE_DOWN!"
              );
            }
            var dc = args.get(:dc);
            args.remove(:dc);
            dcXy = [dc.getWidth(), dc.getHeight() ] as Array<Numeric>;
          }
        }

        protected function initializeSlideDown() {
          self.hideCoords = [
            0,
            self.showCoords[1] - self.canvas[1]
          ] as Array<Numeric>;

          if (hideCoords[1] > 0) {
            self.hideCoords[1] -= showCoords[1];
          }
        }

        protected function initializeSlideUp() {
          self.hideCoords = [ 0, dcXy[1] ] as Array<Numeric>;
        }

        protected function initializeSlideRight() {
          self.hideCoords = [
            self.showCoords[0] - self.canvas[0],
            0,
          ] as Array<Numeric>;

          if (hideCoords[0] > 0) {
            self.hideCoords[0] -= showCoords[0];
          }
        }

        protected function initializeSlideLeft() {
          self.hideCoords = [ dcXy[0], 0 ] as Array<Numeric>;
        }

        protected function transitionSlideDown() as Boolean {
          return transitionSlideZeroCoords(:y);
        }

        protected function transitionSlideUp() as Boolean {
          return transitionSlideMaxCoords(:y);
        }

        protected function transitionSlideRight() as Boolean {
          return transitionSlideZeroCoords(:x);
        }

        protected function transitionSlideLeft() as Boolean {
          return transitionSlideMaxCoords(:x);
        }

        private function transitionSlideZeroCoords(
          xy as Lang.Symbol
        ) {

          var get, set, show, hide;
          if (xy == :x) {
            get  = method(:getX);
            set  = method(:setX);
            show = showCoords[0];
            hide = hideCoords[0];
          }
          else {
            get = method(:getY);
            set = method(:setY);
            show = showCoords[1];
            hide = hideCoords[1];
          }

          var cur = get.invoke();
          var last = false;

          if(becomes && cur <= show) {
            var e = cur + self.increments;
            if (e > show) {
              e = show;
            }
            if (e == show) {
              last = true;
            }
            set.invoke(e.toNumber());
          }
          else if (!becomes && cur >= hide) {
            var e = cur - self.increments;
            if (e < hide) {
              e = hide;
            }
            if (e == hide) {
              last = true;
            }
            set.invoke(e.toNumber());
          }
          return last;
        }

        private function transitionSlideMaxCoords(
          xy as Lang.Symbol
        ) {

          var get, set, show, hide;
          if (xy == :x) {
            get  = method(:getX);
            set  = method(:setX);
            show = showCoords[0];
            hide = hideCoords[0];
          }
          else {
            get = method(:getY);
            set = method(:setY);
            show = showCoords[1];
            hide = hideCoords[1];
          }

          var cur = get.invoke();
          var last = false;

          if(becomes && cur >= show) {
            var e = cur - self.increments;
            if (e < show) {
              e = show;
            }
            if (e == show) {
              last = true;
            }
            set.invoke(e.toNumber());
          }
          else if (!becomes && cur <= hide) {
            var e = cur + self.increments;
            if (e > hide) {
              e = hide;
            }
            if (e == hide) {
              last = true;
            }
            set.invoke(e.toNumber());
          }
          return last;
        }
      }
    }
  }
}
