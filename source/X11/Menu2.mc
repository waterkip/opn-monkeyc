// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi as Ui;
import Toybox.Lang;
using Toybox.System as Sys;

// Module: OPN.X11.Menu2

// Annotation: gfx

module OPN {

  (:gfx)
  module X11 {

    module Menu2 {

      // Function: reloadInPlace(menu, index, delegate, action)
      // A bit of a hack because..  The problem with popView() is that it
      // doesn't reload anything because there isn't an onShow() trigger. You
      // cannot request an update because we are still in *this* view. The work
      // around is.. to have the previous menu call the menu itself to get a
      // onShow().
      public function reloadInPlace(menu, index as Lang.Boolean or Lang.Symbol, delegate, action) {

        // So here we POP the view, nothing gets reloaded
        Ui.popView(Ui.SLIDE_IMMEDIATE);

        // We set the focus on the previous selected item if requested to mimic
        // the popView behaviour
        if (index instanceof Lang.Symbol) {
          setItemFocus(menu, index);
        }

        // This function accepts a boolean, but only true is allowed, the false
        // would have been/show have been caught by whoever called us.
        Ui.switchToView(menu, delegate, action);
        return;
      }

      // Function: setItemFocus(menu, id)
      // Set the focus on an item based on your ID
      public function setItemFocus(menu, id) {
        var itemId = menu.findItemById(id);
        menu.setFocus(itemId);
      }

      // Function: setItemLabel(menu, id)
      // Set the label on an item based on your ID
      public function setItemLabel(menu, id, label) {
        var itemId = menu.findItemById(id);
        var item   = menu.getItem(itemId);
        item.setLabel(label);
        menu.updateItem(item, itemId);
      }

      // Function: setItemSublabel(menu, id)
      // Set the sublabel on an item based on your ID
      public function setItemSublabel(menu, id, sublabel) {
        var itemId = menu.findItemById(id);
        var item   = menu.getItem(itemId);
        item.setSubLabel(sublabel);
        menu.updateItem(item, itemId);
      }

      // Function: setItemLabels(menu, id)
      // Set both the label and sublabel on an item based on your ID
      public function setItemLabels(menu, id, label, sublabel) {
        var itemId = menu.findItemById(id);
        var item   = menu.getItem(itemId);
        item.setLabel(label);
        item.setSubLabel(sublabel);
        menu.updateItem(item, itemId);
      }

      // Function: deleteItem(menu, id)
      // Delete an item from a menu, but doesnt complain if its missing
      public function deleteItem(menu, id) {
        var item = menu.findItemById(id);
        // If we cannot find the item, we don't need to delete it
        if (item == -1) {
          return;
        }
        menu.deleteItem(item);
      }
    }
  }
}
