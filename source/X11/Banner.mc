// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi as Ui;
import Toybox.Lang;
using Toybox.Timer;
using OPN.Math.Util as m;

// Module: OPN.X11.Layer

// Annotation: gfx

module OPN {

  (:gfx)
  module X11 {

    module Layer {

      // Class: Banner
      //
      // > import OPN.X11.Layer as Layer;
      // >
      // > var banner = new Layer.Banner({
      // >   :dc => dc,
      // >   :position => :top,
      // > });
      // > addLayer(banner);
      // >
      //
      // Parameters:
      //  - :visibility (default false)
      //  - :dc         (required)
      //  - :position   :top, :middle, :bottom, :left or :right (default :top)
      //  - :height     (defaults to 1/3 on :top, :middle and :bottom)
      //  - :width      (defaults to 2/3 on :left and :right)
      //
      class Banner extends Ui.Layer {

        private var _height as Numeric;
        private var _width  as Numeric;

        private var _dcWidth as Numeric;
        private var _dcHeight as Numeric;

        private var _position as Symbol;

        function initialize(args) {

          if (!args.hasKey(:dc)) {
            throw new Lang.InvalidOptionsException(
              "We require a dc object!"
            );
          }

          if (!args.hasKey(:visibility)) {
            args.put(:visibility, false);
          }

          var dc = args.get(:dc);
          _dcWidth  = dc.getWidth();
          _dcHeight = dc.getHeight();

          if (!args.hasKey(:position)) {
            args.put(:position, :top);
          }

          _position = args.get(:position);

          if (_position == :top || _position == :middle || _position == :bottom) {
            if (!args.hasKey(:width)) {
              args.put(:width, _dcWidth);
            }

            _width = args.get(:width);

            if (!args.hasKey(:height)) {
              args.put(:height, _dcHeight / 3.0);
            }

            _height = args.get(:height);

            if (_position == :top) {
              args.put(:locX, 0);
              args.put(:locY, 0);
            }
            else if (_position == :middle) {
              args.put(:locX, 0);
              args.put(:locY, (_dcHeight / 2) - (_height/2.0));
            }
            else {
              args.put(:locX, 0);
              var x = _dcHeight - _height.format("%.0f").toNumber();
              args.put(:locY, x);
            }

          }
          else if (_position == :left || _position == :right) {
            if (!args.hasKey(:width)) {
              args.put(:width, _dcWidth / 3 * 2);
            }
            _width = args.get(:width);

            if (!args.hasKey(:height)) {
              args.put(:height, _dcHeight);
            }
            _height = args.get(:height);

            if (_position == :left) {
              args.put(:locX, 0);
              args.put(:locY, 0);
            }
            else {
              args.put(:locX, _dcWidth - _width);
              args.put(:locY, 0);
            }

          }
          else {
            throw new Lang.InvalidOptionsException(
              "Invalid _position given!"
            );
          }
          Ui.Layer.initialize(args);
        }
      }
    }
  }
}
