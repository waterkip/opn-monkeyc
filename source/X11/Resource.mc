// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi as Ui;
import Toybox.Lang;

// Module: OPN.X11.Resource

// Annotation: gfx

module OPN {

  (:gfx)
  module X11 {

    module Resource {

      // Function: R(Rez.Strings, :foo)
      //
      // Load a Resource based on the Rez.XXX and the symbol
      // params:
      // - res, Rez.Strings
      // - symbol, :foo
      //
      // > using OPN.X11.Resource as r;
      // > var str = r.R(Rez.Strings, :foo)
      function R(res, symbol as Symbol) as String {
      // This WILL emit
      // Cannot determine if container access is using container type.
        return Rez(res[symbol]);
      }

      // Function: Rez(Res.Strings.XXX)
      //
      // Load a Rez.Strings.XXX resource
      // params:
      // - rez, Rez.Strings.XXX
      //
      // > using OPN.X11.Resource as r;
      // > var str = r.Rez(Rez.Strings.foo)
      function Rez(rez as Symbol) as String {
        return Ui.loadResource(rez);
      }

    }
  }
}
