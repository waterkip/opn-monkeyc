// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi as Ui;
import Toybox.Lang;
using Toybox.Timer;
import Toybox.Graphics;

// Module: OPN.X11.Layer

// Annotation: gfx

module OPN {

  (:gfx)
  module X11 {

    module Layer {

      // Class: AnimatedBaseBanner
      //
      // You should not instantiate this object yourself, you'll most likely
      // need other classes form the animated banner family for this
      //
      // Parameters:
      //  - :visibility (default false)
      //  - :slide      (optional)
      //  - :speed      (default to 50)
      //  - :dc         (optional)
      //  - :steps      (default to 7)
      class AnimatedBaseBanner extends Toybox.WatchUi.Layer {

        protected var transitioning;
        protected var becomes;

        protected var timer;

        protected var hideCoords as Array<Numeric>;
        protected var canvas as Array<Numeric>;
        protected var showCoords as Array<Numeric>;

        protected var dcXy as Array<Numeric>;

        protected var speed;
        protected var steps;
        protected var increments;
        protected var slide as Ui.SlideType?;

        function initialize(args) {

          // We are unlike the normal Layer, not visible by default because
          // we want to transition into the UI
          if (!args.hasKey(:visibility)) {
            args.put(:visibility, false);
          }

          if (args.hasKey(:slide)) {
            var s = args.get(:slide);
            args.remove(:slide);

            // Convert it to a type. Users may pass anything in as long as it
            // becomes a type. We nice, I know.
            s = s as Ui.SlideType;
            assertSlideStyle(s);
            self.slide = s;
          }
          else {
            self.slide = null;
          }

          if (args.hasKey(:dc)) {
            var dc = args.get(:dc);
            if (dc == null) {
              throw new Lang.InvalidOptionsException(
                "Provided empty DC key!"
              );
            }
            if (dc has :getWidth && dc has :getHeight) {
              dcXy = [dc.getWidth(), dc.getHeight()] as Array<Numeric>;
            }
            else {
              throw new Lang.InvalidOptionsException(
                "This doesn't ducktype to a DC!"
              );
            }
          }
          else {
            dcXy = [] as Array<Numeric>;
          }

          Ui.Layer.initialize(args);

          self.timer = null;
          self.becomes = isVisible();
          self.transitioning = false;

          var dc     = getDc();
          canvas     = [ dc.getWidth(), dc.getHeight() ] as Array<Numeric>;
          showCoords = [ getX(), getY() ] as Array<Numeric>;
          hideCoords = [0,0] as Array<Numeric>;

          setSpeed(args.hasKey(:speed) ? args.get(:speed) : null);
          setSteps(args.hasKey(:steps) ? args.get(:steps) : null);

          if (self.slide == Ui.SLIDE_DOWN) {
            initializeSlideDown();
          }
          else if (self.slide == Ui.SLIDE_UP) {
            initializeSlideUp();
          }
          else if (self.slide == Ui.SLIDE_RIGHT) {
            initializeSlideRight();
          }
          else if (self.slide == Ui.SLIDE_LEFT) {
            initializeSlideLeft();
          }

          setVisibleNow(self.becomes);
        }

        protected function assertSlideStyle(s as Ui.SlideType) {
            if (s == Ui.SLIDE_IMMEDIATE) {
              throw new Lang.InvalidValueException(
                "We don't support WatchUi.SLIDE_IMMEDIATE"
              );
            }

            if (s == Ui.SLIDE_BLINK) {
              throw new Lang.InvalidValueException(
                "We don't support WatchUi.SLIDE_BLINK"
              );
            }
        }

        // Function: setSteps(steps)
        // Change the steps or set it to the default when providing null.
        function setSteps(s as Number?) as Void {
          if (s == null) {
            self.steps = 7;
          }
          else {
            self.steps = s;
          }
          self.increments = self.canvas[1] / self.steps;
        }

        // Function: getSteps()
        // Returns the step count
        function getSteps() as Number {
          return self.steps;
        }

        // Function: setSpeed(ms)
        // Change the speed. Cannot be less than 50ms. Smaller is quicker as
        // this influences the transition time. null or lower than 50 sets the
        // default of 50
        function setSpeed(s as Number?) as Void {
          if (s == null || s < 50) {
            self.speed = 50;
            return;
          }
          self.speed = s;
        }

        // Function: getSpeed()
        // Returns the speed count
        function getSpeed() as Number {
          return self.speed;
        }

        // Function: becomesInvisible()
        // Tells you if we become invisible
        function becomesInvisible() {
          return !self.becomes;
        }

        // Function: isTransitioning()
        // Tells you if you are transitioning at this moment
        function isTransitioning() {
          return self.transitioning;
        }

        // Function: setVisible(true)
        // Make the banner visible, triggers the transitioning
        public function setVisible(ynm as Lang.Boolean) as Void {
          if (self.becomes == ynm) {
            return;
          }

          self.transitioning = true;

          if (self.timer == null) {
            self.timer = new Timer.Timer();
          }
          else {
            self.timer.stop();
          }
          self.timer.start(method(:transition), self.speed, true);
          self.becomes = ynm;
          if (ynm == true) {
            Ui.Layer.setVisible(ynm);
          }
          return;
        }

        // Function: setVisibleNow(true)
        // Make the banner visible, without animation
        public function setVisibleNow(ynm as Boolean) as Void {
          if (self.becomes != ynm) {
            self.becomes = ynm;
          }
          if (isVisible() != ynm) {
            Ui.Layer.setVisible(ynm);
          }

          if (ynm) {
            setY(showCoords[1]);
            setX(showCoords[0]);
          }
          else {
            setY(hideCoords[1]);
            setX(hideCoords[0]);
          }
          self.timer = null;
        }

        // This method is only public so the timer has access to it: STAY A-WAY
        // from it in any other capacity
        public function transition() as Void {
          var last = true;
          if (self.slide == Ui.SLIDE_DOWN) {
            last = transitionSlideDown();
          }
          else if (self.slide == Ui.SLIDE_UP) {
            last = transitionSlideUp();
          }
          else if (self.slide == Ui.SLIDE_RIGHT) {
            last = transitionSlideRight();
          }
          else if (self.slide == Ui.SLIDE_LEFT) {
            last = transitionSlideLeft();
          }

          if (last == true) {
            Ui.Layer.setVisible(self.becomes);
            self.transitioning = false;
            self.timer.stop();
            self.timer = null;
          }
          Ui.requestUpdate();
        }

        // Function: initializeSlideDown
        protected function initializeSlideDown() { }
        // Function: initializeSlideUp
        protected function initializeSlideUp() { }
        // Function: initializeSlideRight
        protected function initializeSlideRight() { }
        // Function: initializeSlideLeft
        protected function initializeSlideLeft() { }

        // Function: transitionSlideDown
        protected function transitionSlideDown() as Boolean { return true; }
        // Function: transitionSlideUp
        protected function transitionSlideUp() as Boolean { return true; }
        // Function: transitionSlideRight
        protected function transitionSlideRight() as Boolean { return true; }
        // Function: transitionSlideLeft
        protected function transitionSlideLeft() as Boolean { return true; }

      }
    }
  }
}
