//SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause

import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;

// Module: OPN.X11.Menu2

// Annotation: gfx

module OPN {

  (:gfx)
    module X11 {

      module Menu2 {

        // Class: Delegate
        //
        // Used to start a default delegate that deals with certain topics so
        // you don't have to worry about them. In debug mode it automaticly
        // prints out stuff to the console. In release mode this gets silenced
        //
        // Parameters:
        // - type (defaults to Toybox.WatchUi.SLIDE_IMMEDIATE)
        class Delegate extends Toybox.WatchUi.Menu2InputDelegate {

          protected var itemId;
          protected var itemLabel;
          protected var defaultAction;
          function initialize(type as WatchUi.SlideType?) {
            Menu2InputDelegate.initialize();
            self.itemId    = null;
            self.itemLabel = null;
            if (type == null) {
              type = WatchUi.SLIDE_IMMEDIATE;
            }
            self.defaultAction = type;
          }


          // Function: onSelect(item)
          // exposes self.itemId and self.itemLabel
          function onSelect(item) {
            self.itemId    = item.getId();
            self.itemLabel = item.getLabel();
            diag(
                Lang.format("Selecting $1$ as $2$ ($3$)",
                  [ self.itemLabel, self.itemId.toString(), self.itemId ]
                  )
                );
          }

          // Function: diag(msg)
          // In debug mode this prints stuff to the console
          (:debug)
          protected function diag(msg) { System.println("# " + msg); }
          // Function: diag(msg)
          // In release mode this does nothing
          (:release)
          protected function diag(msg) {}

          // Function: pushView(view, delegate)
          // Sugar for WatchUi.pushView(menu, delegate, defaultAction);
          protected function pushView(menu, delegate) {
            if (menu == null || delegate == null) {
              diag("PushView called with no menu or delegate");
              return;
            }
            WatchUi.pushView(menu, delegate, defaultAction);
          }

          // Function: switchToView(view, delegate)
          // Sugar for WatchUi.switchToView(menu, delegate, defaultAction);
          protected function switchToView(menu, delegate) {
            if (menu == null || delegate == null) {
              diag("SwitchToView called with no menu or delegate");
              return;
            }
            WatchUi.switchToView(menu, delegate, defaultAction);
          }

          // Function: popView()
          // Sugar for WatchUi.popView();
          protected function popView() {
            WatchUi.popView(defaultAction);
          }

          // Function: bailOut(msg)
          // Function quickly bail out of a view with a usefull message
          protected function bailOut(msg) {
            diag(Lang.format("No action defined for $1$ in $2$, popping the view",
                  [ itemLabel, msg ]));

            popView();
          }
        }

      }

    }
}

