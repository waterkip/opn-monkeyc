//
//SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//

import Toybox.Lang;

// Module: OPN.Conversion.Time

// Annotation: time

module OPN {

  (:time)
  module Conversion {

    module Time {

        // Function: toAmPm(time)
        // Converts time to am/pm notation
        //
        // Note:
        // The time object should be compatible with Toybox.System.ClockTime
        function toAmPm(time) {
          var am = "am";

          var hour = time.hour;
          var min  = time.min.format("%02d");
          var sec  = time.sec.format("%02d");

          hour = hour % 12;
          if (time.hour >= 12) {
            am = "pm";
          }

          if (hour == 0) {
            hour = 12;
          }

          return Lang.format("$1$:$2$:$3$ $4$", [hour, min, sec, am]);
        }

        // Function: iso8601(time)
        // Converts time to an ISO8601 format, HH:MM:ss
        //
        // Note:
        // The time object should be compatible with Toybox.System.ClockTime
        function iso8601(time) {
          return Lang.format("$1$:$2$:$3$", [
              time.hour.format("%02d"),
              time.min.format("%02d"),
              time.sec.format("%02d")
          ]);
        }

        // Function: seconds2HMS(seconds)
        // Convert seconds to HH:MM:ss
        function seconds2HMS(seconds) {

          var sec = seconds % 60;
          var minutes = seconds / 60;
          var min = minutes % 60;
          // we should divide by 24 hrs to get to days
          var hours = minutes / 60;

          return Lang.format("$1$:$2$:$3$", [
              hours.format("%02d"),
              min.format("%02d"),
              sec.format("%02d")
          ]);

        }
    }
  }
}
