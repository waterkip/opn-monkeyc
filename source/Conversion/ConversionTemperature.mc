//
//SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//

import Toybox.Lang;

// Module: OPN.Conversion.Temperature

// Annotation: temp

module OPN {
  (:temp)
  module Conversion {
    module Temperature {

      // Function: toF(celcius)
      // Convert celcius to fahrenheit
      function toF(c) {
        return (c.toDouble() * 9/5) + 32;
      }

      // Function: toFahrenheit(celcius)
      // If you want to write more than toF()
      function toFahrenheit(c) {
        return toF(c);
      }

    }
  }
}
