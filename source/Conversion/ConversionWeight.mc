//SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause

import Toybox.Lang;

// Module: OPN.Conversion.Weight

// Annotation: weight

module OPN {

  (:weight)
  module Conversion {
    module Weight {
      // Function: toLbs(kg)
      // Convert kilo's to pounds
      function toLbs(kg) as Double {
        return kg.toDouble() * 2.2046226218488d;
      }
    }
  }
}
