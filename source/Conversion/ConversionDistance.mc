//
//SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause
//

import Toybox.Lang;

// Module: OPN.Conversion.Distance

// Annotation: dist

module OPN {
  (:dist)
  module Conversion {
    module Distance {
      // Function: toMiles(km)
      // Convert km to miles
      function toMiles(km) {
        return km.toDouble() * 0.6213711922d;
      }
    }
  }
}
