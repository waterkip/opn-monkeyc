//SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opperschaap.net>
//SPDX-FileCopyrightText: 2022 Nichole Danser <nichole@scubblebubbles.org>
//
//SPDX-License-Identifier: BSD-3-Clause

import Toybox.Lang;
using Toybox.Math as m;
using Toybox.System as Sys;
using OPN.Math.Util as u;

// Module: OPN.Math.Coordinates

// Annotation: math

module OPN {

  (:math)
  module Math {

    module Coordinates {

      /*
        Function: getBoxCoords(args)

        Parameters:
          * :size, required
          * :canvas, optional
          * :degrees, optional
          * :hours, optional
          * :minutes, optional
          * :radians, optional

        You need to supply at least one of :degrees, :hours, :minutes, :radians
      */
      function getBoxCoords(shim) as Array<Lang.Numeric> {
        var degrees = null;
        var radians = null;

        if (shim.hasKey(:degrees)) {
          degrees = shim.get(:degrees);
        }
        else if (shim.hasKey(:hours)) {
          degrees = shim.get(:hours) * 30;
        }
        else if (shim.hasKey(:hour)) {
          degrees = shim.get(:hour) * 30;
        }
        else if (shim.hasKey(:minutes)) {
          degrees = shim.get(:minutes) * 6;
        }
        else if (shim.hasKey(:minute)) {
          degrees = shim.get(:minute) * 6;
        }
        else if (shim.hasKey(:radians)) {
          radians = shim.get(:radians).toDouble();
        }
        else {
          throw new Lang.InvalidOptionsException(
            "You need to supply a unit of measurement!"
          );
        }

        var centerX;
        var centerY;
        var radius;

        var width;
        var height;
        if (shim.hasKey(:size)) {
          var size = shim.get(:size);
          var canvas;
          if (shim.hasKey(:canvas)){
            canvas = shim.get(:canvas);
          }
          else {
            canvas = size;
          }

          width  = size;
          height = size;

          centerX = canvas / 2;
          centerY = canvas / 2;
          radius  = size / 2;
        }
        else {
          throw new Lang.InvalidOptionsException(
            "You need to supply a size/canvas!"
          );
        }

        centerX  = centerX.toDouble();
        centerY  = centerY.toDouble();

        width  = width.toDouble();
        height = height.toDouble();

        if (radians == null) {
          radians = m.toRadians(degrees.toDouble());
        }

        var x = m.sin(radians);
        var y = m.cos(radians);

        var M = u.max(u.abs(x), u.abs(y));

        x = centerX + radius * x / M;
        y = centerY - radius * y / M;

        return [ x, y ] as Array<Lang.Numeric>;
      }

      /*
        Function: getCircleCoords(args)

        Parameters:
          * :size, required
          * :canvas, optional
          * :degrees, optional
          * :hours, optional
          * :minutes, optional
          * :radians, optional

        You need to supply at least one of :degrees, :hours, :minutes, :radians
      */
      function getCircleCoords(shim) as Array<Lang.Numeric> {

        var degrees = null;
        var radians = null;

        if (shim.hasKey(:degrees)) {
          degrees = shim.get(:degrees);
        }
        else if (shim.hasKey(:hours)) {
          degrees = shim.get(:hours) * 30;
        }
        else if (shim.hasKey(:hour)) {
          degrees = shim.get(:hour) * 30;
        }
        else if (shim.hasKey(:minutes)) {
          degrees = shim.get(:minutes) * 6;
        }
        else if (shim.hasKey(:minute)) {
          degrees = shim.get(:minute) * 6;
        }
        else if (shim.hasKey(:radians)) {
          radians = shim.get(:radians).toDouble();
        }
        else {
          throw new Lang.InvalidOptionsException(
            "You need to supply a unit of measurement!"
          );
        }

        var centerX;
        var centerY;
        var radius;
        if (shim.hasKey(:size)) {
          var size = shim.get(:size);
          var canvas;
          if (shim.hasKey(:canvas)){
            canvas = shim.get(:canvas);
          }
          else {
            canvas = size;
          }
          centerX = (canvas / 2).toDouble();
          centerY = (canvas / 2).toDouble();
          radius  = (size   / 2).toDouble();
        }
        else if (shim.hasKey(:radius) ) {
          radius = shim.get(:radius);
          centerX = shim.get(:x);
          centerY = shim.get(:y);
        }
        else {
          throw new Lang.InvalidOptionsException(
            "You need to supply a size and radius with x/y coordinates!"
          );
        }

        if (degrees != null) {
          radians = m.toRadians(degrees.toDouble());
        }

        // https://www.youtube.com/watch?v=SbuqMbvzsgs
        var x = centerX + radius * m.sin(radians);
        var y = centerY - radius * m.cos(radians);
        return [ x, y ] as Array<Lang.Numeric>;
      }
    }
  }
}
