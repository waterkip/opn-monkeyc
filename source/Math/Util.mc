//SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opperschaap.net>
//
//SPDX-License-Identifier: BSD-3-Clause

import Toybox.Lang;
using Toybox.Math;
using Toybox.System as Sys;

// Module: OPN.Math.Util

// Annotation: math

module OPN {

  (:math)
  module Math {

    module Util {

      // Function: seedRNG()
      // Seed the random number generator. Only call this function once,
      // preferable at your app start up
      function seedRNG() {
        var time = Sys.getClockTime();
        time = Lang.format("$1$$2$$3$", [ time.hour, time.min.format("%02d"), time.sec.format("%02d") ]).toNumber();
        time *= Math.rand();
        time *= Math.rand();
        Math.srand(time);
      }

      // Function: randomChar()
      // Get a random character (Toybox.Lang.Char) between 0 and 255.
      function randomChar() as Lang.Char {
        var r = 256;
        while (r > 255) {
          r = Math.rand();
          r %= 1000;
        }
        return r.toChar();
      }

      // Function: abs(x)
      // Returns the absolute value of a number
      function abs(x as Lang.Numeric) as Lang.Numeric {
        if (x < 0) {
           return x * -1;
        }
        return x;
      }

      // Function: max(a, b)
      // Returns the highest number of the two
      function max(x as Lang.Numeric, y as Lang.Numeric) as Lang.Numeric {
        return x > y ? x : y;
      }
    }
  }
}
