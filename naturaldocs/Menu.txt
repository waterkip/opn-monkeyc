Format: 1.51


Title: OPN
SubTitle: A MonkeyC barrel by waterkip

Footer: Documentation is provided for free
Timestamp: Generated on yyyy-mm-dd
#   m     - One or two digit month.  January is "1"
#   mm    - Always two digit month.  January is "01"
#   mon   - Short month word.  January is "Jan"
#   month - Long month word.  January is "January"
#   d     - One or two digit day.  1 is "1"
#   dd    - Always two digit day.  1 is "01"
#   day   - Day with letter extension.  1 is "1st"
#   yy    - Two digit year.  2006 is "06"
#   yyyy  - Four digit year.  2006 is "2006"
#   year  - Four digit year.  2006 is "2006"


# --------------------------------------------------------------------------
# 
# Cut and paste the lines below to change the order in which your files
# appear on the menu.  Don't worry about adding or removing files, Natural
# Docs will take care of that.
# 
# You can further organize the menu by grouping the entries.  Add a
# "Group: [name] {" line to start a group, and add a "}" to end it.
# 
# You can add text and web links to the menu by adding "Text: [text]" and
# "Link: [name] ([URL])" lines, respectively.
# 
# The formatting and comments are auto-generated, so don't worry about
# neatness when editing the file.  Natural Docs will clean it up the next
# time it is run.  When working with groups, just deal with the braces and
# forget about the indentation and comments.
# 
# --------------------------------------------------------------------------


Link: Repository  (https://gitlab.com/waterkip/opn-monkeyc/-/tree/master)
Link: Changelog  (https://gitlab.com/waterkip/opn-monkeyc/-/blob/master/Changelog.md)
Link: README  (https://gitlab.com/waterkip/opn-monkeyc/-/blob/master/README.md)

Group: Index  {

   Index: Everything
   Annotation Index: Annotations
   Function Index: Functions
   Module Index: Modules
   Class Index: Classes
   }  # Group: Index

Group: Test  {

   File: OPN.Test.More  (no auto-title, Test/More.mc)
   }  # Group: Test

Group: Conversion  {

   File: OPN.Conversion.Distance  (no auto-title, Conversion/ConversionDistance.mc)
   File: OPN.Conversion.Temperature  (no auto-title, Conversion/ConversionTemperature.mc)
   File: OPN.Conversion.Time  (no auto-title, Conversion/ConversionTime.mc)
   File: OPN.Conversion.Weight  (no auto-title, Conversion/ConversionWeight.mc)
   }  # Group: Conversion

Group: Math  {

   File: OPN.Math.Coordinates  (no auto-title, Math/Coordinates.mc)
   File: OPN.Math.Util  (no auto-title, Math/Util.mc)
   }  # Group: Math

Group: Generate  {

   File: OPN.Generate.UUID  (no auto-title, Generate/UUIDv4.mc)
   }  # Group: Generate

Group: X11  {

   File: OPN.X11.Layer.AnimatedBaseBanner  (no auto-title, X11/AnimatedBaseBanner.mc)
   File: OPN.X11.Layer.AnimatedBanner  (no auto-title, X11/AnimatedBanner.mc)
   File: OPN.X11.Layer.Banner  (no auto-title, X11/Banner.mc)
   File: OPN.X11.Menu2  (no auto-title, X11/Menu2.mc)
   File: OPN.X11.Menu2Delegate  (no auto-title, X11/Menu2Delegate.mc)
   File: OPN.X11.ViewX  (no auto-title, X11/ViewX.mc)
   File: OPN.X11.Resource  (no auto-title, X11/Resource.mc)
   }  # Group: X11

