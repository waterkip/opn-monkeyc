OPN-MonkeyC v0.0.6
==

Because of the generation of UUID v4, we require API v3.0.0 for our consumers.

Fore more information please visit the documenation at:

https://waterkip.gitlab.io/opn-monkeyc/

# Install barrel in your project

In your monkey.jungle:

```
OPN = barrels/OPN.barrel
# We need to import all annotations because of:
# https://forums.garmin.com/developer/connect-iq/i/bug-reports/bug-barrel-with-several-annotations-cannot-have-tests
base.OPN.annotations = temp;time;weight;dist
# These three are all needed because of
# https://forums.garmin.com/developer/connect-iq/i/bug-reports/rfc-allow-test-annotation-in-a-barrel-to-exist-without-release-and-debug-annotation
base.OPN.annotations = $(base.OPN.annotations);test;release;debug
base.barrelPath = $(base.barrelPath);$(OPN)
```

In your manifest.xml:

```
    <iq:barrels>
      <iq:depends name="OPN" version="0.0.2"/>
    </iq:barrels>
```

# Development on this barrel

This barrel uses the Garmin Connect SDK Docker project[^1][^2]. Please see the
README there to get that going.

## Building the barrel

The easiest way to build the barrel is to run the following docker-compose
command:

```
docker-compose run --rm connectiq make barrel
mv bin/OPN.barrel /path/to/other/project
```

## Versions

The current version in the master branch is *always* one more than we have
tagged. Eg, when the latest tag is v0.0.2, the version of the barrel in the
master branch is 0.0.3.

When building a specific version, checkout a tag and build the barrel.

[^1]: https://gitlab.com/waterkip/garmin-connectiq-sdk-docker/-/tree/master
[^2]: https://github.com/waterkip/connectiq-sdk-docker
